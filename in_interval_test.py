import pytest

from in_interval import in_interval


@pytest.mark.parametrize(
    "number, start, end, expected",
    [
        (5, 1, 10, True),
        (0, 0, 0, True),
        (10, 1, 10, True),
        (1, 1, 10, True),
        (10, 1, 9, False),
        (-5, 1, 10, False),
        (11, 1, 10, False),
    ],
)
def test_in_interval(number, start, end, expected):
    """
    Test the in_interval function with various inputs.
    """
    assert in_interval(number, start, end) == expected


@pytest.mark.parametrize(
    "number, start, end",
    [
        (5, 10, 1),
        (0, 10, 0),
        (10, 10, 1),
        (1, 10, 1),
        (10, 9, 1),
        (-5, 10, 1),
        (11, 10, 1),
    ],
)
def test_start_greater_than_end(number, start, end):
    """
    Test that the function raises a ValueError when start is greater than end.
    """
    with pytest.raises(ValueError):
        in_interval(number, start, end)
