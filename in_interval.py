def in_interval(number: int, start: int, end: int) -> bool:
    """
    Check if a number is within a given interval (inclusive).

    Args:
        number (int): The number to check.
        start (int): The start of the interval.
        end (int): The end of the interval.

    Returns:
        bool: True if the number is within the interval, False otherwise.
    """
    if start > end:
        raise ValueError("Start must be less than or equal to end")

    return start <= number <= end
