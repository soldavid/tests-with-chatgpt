import pytest

from is_prime import is_prime


@pytest.mark.parametrize(
    "number, expected",
    [
        (2, True),
        (3, True),
        (4, False),
        (5, True),
        (7, True),
        (10, False),
        (11, True),
        (17, True),
        (20, False),
        (29, True),
    ],
)
def test_is_prime(number, expected):
    """
    Test the is_prime function with various inputs.
    """
    assert is_prime(number) == expected


def test_negative_number():
    """
    Test that the is_prime function returns False for negative numbers.
    """
    assert is_prime(-5) is False


def test_zero():
    """
    Test that the is_prime function returns False for zero.
    """
    assert is_prime(0) is False


def test_one():
    """
    Test that the is_prime function returns False for one.
    """
    assert is_prime(1) is False
