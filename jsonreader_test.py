import json

import pytest

from jsonreader import JSONReader


@pytest.fixture
def json_file(tmp_path):
    """
    Fixture to create a temporary JSON file for testing.
    """
    data = {"name": "John Doe", "age": 30}
    file_path = tmp_path / "test_data.json"
    with open(file_path, "w", encoding="utf-8") as file:
        json.dump(data, file)
    return file_path


@pytest.fixture
def json_reader(json_file):
    """
    Fixture to create a JSONReader instance with the temporary JSON file.
    """
    return JSONReader(json_file)


def test_read_json(json_reader):
    """
    Test that the JSONReader can read the JSON file correctly.
    """
    data = json_reader.read()
    assert data == {"name": "John Doe", "age": 30}


def test_file_not_found(tmp_path):
    """
    Test that the JSONReader raises a FileNotFoundError when the file doesn't exist.
    """
    non_existent_file = tmp_path / "non_existent.json"
    with pytest.raises(FileNotFoundError):
        JSONReader(non_existent_file).read()
