# Write Unit Tests for Your Python Code With ChatGPT

Taken from [Real Python](https://realpython.com/chatgpt-unit-tests-python/)

## Run Tests

```bash
pytest --junitxml=test_report.xml --html=public/index.html
```
