import pytest  # noqa: F401

from fizzbuzz import fizzbuzz


@pytest.mark.parametrize(
    "input_num, expected_output",
    [
        (3, "fizz"),
        (6, "fizz"),
        (9, "fizz"),
    ],
)
def test_fizz(input_num, expected_output):
    assert fizzbuzz(input_num) == expected_output


@pytest.mark.parametrize(
    "input_num, expected_output",
    [
        (5, "buzz"),
        (10, "buzz"),
        (20, "buzz"),
    ],
)
def test_buzz(input_num, expected_output):
    assert fizzbuzz(input_num) == expected_output


@pytest.mark.parametrize(
    "input_num, expected_output",
    [
        (15, "fizz buzz"),
        (30, "fizz buzz"),
        (45, "fizz buzz"),
    ],
)
def test_fizzbuzz(input_num, expected_output):
    assert fizzbuzz(input_num) == expected_output


@pytest.mark.parametrize(
    "input_num, expected_output",
    [
        (1, 1),
        (2, 2),
        (7, 7),
    ],
)
def test_other_numbers(input_num, expected_output):
    assert fizzbuzz(input_num) == expected_output
